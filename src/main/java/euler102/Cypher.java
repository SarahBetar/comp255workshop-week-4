
package org.bitbucket.euler102;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Provides methods for encryption, decryption and key finding.
 */
public class Cypher {

  /**
   *  Logger for this class
   */
  static Logger logger = LoggerFactory.getLogger(Cypher.class);

  /**
   *  Decypher unencrypts an encrypted string.
   *
   *  @param s      The encrypted string.
   *  @param key    The key used to encrypt string s.
   *
   *  @return       String s, decrypted.
   */
  public static byte[] decypher(String s, String key) {
    return cypher(s, key);
  }

  /**
   * Encrypt a string using a given key.
   *
   * @param s       The string to encrypt.
   * @param key     The key.
   *
   * @return        The encryption of `s` using `key`.
   */
  public static byte[] cypher(String s, String key) {

    byte[] sourceString = new byte[ s.length() ];
    byte[] keyString = new byte[ key.length() ];
    byte[] encocedString = new byte[ s.length() ];
    // Xor the char with the key cyclically
    try {
      sourceString = s.getBytes("US-ASCII");
      keyString = key.getBytes("US-ASCII");

      for (int i = 0; i < s.length(); i++) {
        encocedString[i] = (byte)(sourceString[i] ^ keyString[i % key.length()]);
      }

    } catch (Exception e) {
      // Log exception
      logger.error("String {} could not be cyphered:  {}", s, e.getMessage());
    }
    logger.info("Encoding is {}", encocedString);

    //  return the String that corresponds to bytes
    return encocedString;
  }

  /**
   * Given two bytes, source and target, find the key used to encrypt source into target.
   *
   * @param source      The byte being checked, before encryption.
   * @param target      The source byte, encrypted.
   *
   * @return            The key used to encrypt source into string target.
   */
  private static char findKey(byte source, byte target) {

    char result = ' ';

    for (byte b = -128; b < 127; b++) {
      // If (b xor source) is equal to target, we have found the key
      if ((b ^ source) == target) {
        logger.info("Found key! {}, {}", b, (char)b);
        result = (char)b;
      }
    }
    return result;
  }

    /**
     * Given two strings, source and target, and a length, find the key used to encrypt the first 'length' bytes of source into target.
     *
     * @param length      The number of bytes in source to be checked.
     * @param source      The string being checked, before encryption.
     * @param target      The source string, encrypted.
     *
     * @return            The key used to encrypt the first 'length' bytes of source.
     */

  public static String findKey(int length, String source, String target) {
    char[] keyString = new char[length];
    try {
      byte[] byteSource = source.getBytes("US-ASCII");
      byte[] byteTarget = target.getBytes("US-ASCII");

      for(int i = 0; i < length; i++) {
        keyString[i] = findKey(byteSource[i], byteTarget[i]);
      }
    }
    catch (Exception e) {
      logger.error("String {} could not be cyphered:  {}", source, e);
    }

    return new String(keyString);
  }

}
